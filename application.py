# -*- coding: utf-8 -*-
import os
from flask import *
import urllib2
import datetime

app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = 'secret'

slownik = {"users":[], "news":[]}
server = {}


def save_base():
    f=open(os.path.join('static', 'baza.json'), "w+")
    f.write(json.dumps(slownik))

def load_base():
    print "loaded"
    f1=open(os.path.join('static', 'baza.json'), "r+")
    str2 =  f1.read()
    global slownik
    slownik = json.loads(str2)
    print len(slownik["images"])

@app.route('/')
def index():
    logged = False
    if "logged" in session:
        logged = True
    users = slownik["users"]
    return render_template('index.html', logged=logged, users = users)


@app.route('/login', methods=['GET', 'POST'])
def login():
    info = ""
    # load_base()
    if request.method == "POST":
        login = request.form['loginValue']
        if login in slownik["users"]:
            info = "User already exists!"
            return render_template('login.html', info = info)
        else:
            print slownik["users"]
            slownik["users"].append(login)
            session['logged'] = True
            session['loggedUser'] = login
            info = "You are logged now!!!"
            # return render_template('index.html', info = info, logged = True)
            return redirect("")
    if request.method == "GET":
        info = ""
        if "logged" in session:
            info = "You are already logged!"
            # return render_template('index.html', info = info, logged=True)
            return redirect("")
        return render_template('login.html', info = info)
#
@app.route('/logout')
def logout():
    info = "Some error"
    if "logged" in session:
        if session["loggedUser"] in slownik["users"]:
            slownik["users"].remove(session["loggedUser"])
        session.pop('logged', None)
        session.pop('loggedUser', None)
        info = "logout!"
    return render_template('index.html', info = info, logged = False)

@app.route('/chat')
def chat():
    if "logged" in session:
        users = slownik["users"]
        news=slownik["news"]
        return render_template('chat.html', users = users, news=news)
    return render_template('login.html')

@app.route('/sendChat', methods=['POST'])
def sendChat():
    if request.method == "POST":
        if "logged" in session:
            entry = request.form['value']
            now = datetime.datetime.now()
            slownik["news"].append(str(now.hour)+":"+str(now.minute)+":"+str(now.second)+" "+session["loggedUser"]+": "+entry)
            if len(slownik["news"]) >10:
                slownik["news"].pop(0)
            users = slownik["users"]
            news = slownik["news"]
            #return render_template('index.html', users = users, news=news)
            return redirect("")
        # return render_template('login.html')

@app.route('/getChat', methods=['GET', 'POST'])
def getChat():
    if request.method == "GET":
        return render_template('other_server.html')
        # return render_template('login.html')
    if request.method == "POST":
        id = request.form['id']
        # print "cos"
        user = request.form['user']
        message = request.form['message']
        timestamp = request.form['timestamp']
        slownik["news"].append(timestamp+" "+user+": "+message)
        if len(slownik["news"]) >10:
            slownik["news"].pop(0)
        return redirect("")
#
# @app.route('/add', methods=['GET', 'POST'])
# def add():
#     info = ""
#     if request.method == "POST":
#         if "logged" in session:
#             print "kupka"
#             load_base()
#             path = request.form['pathValue']
#             slownik["images"].append(path)
#             save_base()
#             info = "Added"
#             images = slownik["images"]
#             return render_template('images.html', info = info, images = images)
#     if request.method == "GET":
#         if "logged" in session:
#             info = "Add image"
#             return render_template('add_image.html', info = info)
#         if "logged" not in session:
#             info = "You must login"
#             return render_template('index.html', info = info, logged = False)
#

if __name__ == '__main__':
    app.run(debug=True)

